+++
title = "Weekly Update (11/2022): A RISC-V handheld, Fedora on the PinePhone Pro, Precursor shipping and new releases of SailfishOS, libhandy and libadwaita"
date = "2022-03-19T22:00:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","Librem 5","Fedora","Precursor","DevTerm","RISC-V","libhandy","libadwaita","Sailfish OS", ]
categories = ["weekly update"]
authors = ["peter"]
+++

Eight days of news, and still not a lot that's super Linux on Mobile specific. At least there's been another Ubuntu Touch Q&A, another postmarketOS Service Pack, and it looks like that really need to find the time to have a look at Capyloon.
<!-- more -->

_Commentary in italics._

### Hardware

#### Amazingly well thought out Raspberry Pi based hardware
* Liliputing: [Pockit is a tiny, modular computer with dozens of blocks that snap into place to add functionality](https://liliputing.com/2022/03/pockit-is-a-tiny-modular-computer-with-dozens-of-blocks-that-snap-into-place-to-add-functionality.html). _This is so well made. Amazing._

#### RISC-V handheld device
* CNX Software: [DevTerm modular Linux terminal gets a RISC-V module compatible with Raspberry Pi CM3](https://www.cnx-software.com/2022/03/16/devterm-modular-linux-terminal-risc-v-module-raspberry-pi-cm3/). _It's powered by an [AllWinner D1 SoC](https://linux-sunxi.org/D1), which is powered by a XuanTie C906 core. ClockworkPi also offer their Raspberry Pi 3 Compute module compatible SOM quite affordably, but the D1 might not be the RISC-V chip you want for reasons [discussed on Hacker News](https://news.ycombinator.com/item?id=30689030)._

#### Crowdfunded devices shipping
* Liliputing: [Precursor open hardware mobile device with an FPGA is now shipping (crowdfunding)](https://liliputing.com/2022/03/precursor-open-hardware-mobile-device-with-an-fpga-is-now-shipping-crowdfunding.html). _Nice to see it shipping, it was [announced in September 2020](https://linmob.net/linbits11-weekly-pinephone-news-week38/#other-hardware)._

### Hardware enablement
* Liliputing: [Ubuntu Touch could breathe new life into the troubled JingPad A1 Linux tablet](https://liliputing.com/2022/03/ubuntu-touch-could-breathe-new-life-into-the-troubled-jingpad-a1-linux-tablet.html).

#### Firmware
* U-Boot fixes for the PinePhone Pro have landed, [which enable the proximity and light sensor to work](https://twitter.com/DanctNIX/status/1504660410200629253#m).

#### GNOME ecosystem
* Alexander Mikhaylenko: [Libadwaita 1.1, Libhandy 1.6](https://blogs.gnome.org/alexm/2022/03/19/libadwaita-1-1-libhandy-1-6/). _Great new releases!_
* This Week in GNOME: [#35 Software Reviews](https://thisweek.gnome.org/posts/2022/03/twig-35/). _I wonder how well Symphony works on a phone..._


#### Plasma/Maui ecosystem
* Nate Graham: [This week in KDE: fewer korners, more multi-cursor, better apps](https://pointieststick.com/2022/03/11/this-week-in-kde-fewer-korners-more-multi-cursor-better-apps/).
* Nate Graham: [This week in KDE: Back to those 15-minute bugs](https://pointieststick.com/2022/03/18/this-week-in-kde-back-to-those-15-minute-bugs/).
* Volker Krause: [KDE Itinerary @ Wikidata Data Reuse Days 2022](https://www.volkerkrause.eu/2022/03/12/kde-itinerary-wikidata-data-reuse-days-2022.html).
* KDE Eco: [First Ever Eco-Certified Computer Program: KDE's Popular PDF Reader Okular](https://eco.kde.org/blog/2022-03-16-press-release-okular-blue-angel/).
* Qt blog: [Qt for WebAssembly on mobile devices](https://www.qt.io/blog/qt-for-webassembly-on-mobile-devices). _WebAssembly is a way to make use of Qt 6* on devices where it's hard or impossible to use Qt otherwise, e.g. iOS devices._
* Nitrux: [Maui Shell Dev Diaries](https://nxos.org/weekly-summaries/maui-shell-dev-diaries/). _I am really looking forward to Maui Shell!_



#### Distro releases
* An unofficial [Fedora image for the PinePhone Pro](https://github.com/nikhiljha/pp-fedora-sdsetup/releases/tag/ppp-0) has been released.
* [postmarketOS 21.12 Service Pack 3](https://postmarketos.org/blog/2022/03/13/v21.12.3-release/) has been released, featuring new builds Dino, Portfolio and kernel updates for a few devices.
* Dan Johansen: [This month in Manjaro (February 2022)](https://blog.strits.dk/this-month-in-manjaro-february-2022/). _Nice progress!_


#### Sailfish OS
* jovirkku: [[Release notes] Vanha Rauma 4.4.0 ](https://forum.sailfishos.org/t/release-notes-vanha-rauma-4-4-0/10656). _Notable browser and camera improvements, and a bunch of bug and CVE fixes. Make sure to watch Leszek's video below!_

#### UBports
* UBports: [Your biweekly UBports news with a (buggy) new design](http://ubports.com/blog/ubports-news-1/post/your-biweekly-ubports-news-with-a-buggy-new-design-3840). _Foundation Elections, a UBports Teach team, new apps, and more._

#### Capyloon
* Capyloon (B2G/Firefox OS reboot) has also had [another release](https://capyloon.org/releases.html), featuring a number of changes important for privacy.

### Worth noting
* If you're tired of customizing your GSettings on your Linux Phone by over and over again, [you can now use Ansible](https://twitter.com/jistr/status/1502774618784165888).
* [GNUnet messenger is getting closer to release](https://twitter.com/TheJackiMonster/status/1502771537253912577/).


### Worth reading

#### PINE64 Community Update
* PINE64: [March Update: Introducing the QuartzPro64](https://www.pine64.org/2022/03/15/march-update-introducing-the-quartzpro64/).

#### First Impressions of Linux Phones
* Nirik: [Pinephone pro](https://www.scrye.com/wordpress/nirik/2022/03/17/pinephone-pro/). _Nice first impressions with a focus on upstreamability!_
* etbe: [Librem 5 First Impression](https://etbe.coker.com.au/2022/03/15/librem-5-first-impression/). _I have been pondering writing a follow-up post on how the Librem 5 is doing now (I've been using it for the past week,[^1] and it did good enough for me to not go back to the PinePhone), and that GNOME Keyring thing is something I'd spend a paragraph or two on as well._

#### PowerVR Mesa Driver
* TuxPhones: [What the new PowerVR driver means for mobile](https://tuxphones.com/what-does-the-new-powervr-driver-mean-for-mobile/). _Great post by Martijn!_

#### Not a new device, just a cellular plan
* Purism: [Purism Launches Librem SIMple: A Cellular Service that Protects Digital Privacy](https://puri.sm/posts/purism-launches-librem-simple/). _Affordability, like so many things in life, is a relative term._

### Worth watching

#### PinePhone (Pro) Impressions
* Yomen Tohmaz: [PinePhone Review: Half a Year In](https://www.youtube.com/watch?v=HbiObOy3YsA).
* Canadian Bitcoiners: [PinePhone Pro Update - Almost Ready For Daily Driver](https://www.youtube.com/watch?v=9z7oaTLBfsI). _Minor nitpick: The front camera changed since the original announcement to a different part that should be better, but is unsupported in Linux._

#### PINE64 Community Update
* PINE64: [March Update: Introducing the QuartzPro64](https://tilvids.com/w/5nAaa47kxv82MU3kAFsnKE). _Nice video synopsis by PizzaLovingNerd!_

#### Ubuntu Touch
* UBports: [Ubuntu Touch Q&A 117](https://www.youtube.com/watch?v=6bMOb3xtXFA). _Florian and Alfred talk about what's new in Ubuntu Touch: A new installer release, three new devices supported (Lenovo Tab M10 X605, the JingPad, and the ShiftPhones 6mq), VSCodium on the Open-Store and Community Questions._

#### Sailfish 4.4
* Leszek Lesner: [SailfishOS 4.4 - What's new!?](https://www.youtube.com/watch?v=mmgPeK156Yg). _Like with every new SailfishOS release, Leszek did a video to show off what's new._

#### Spanish talks
* Nucleo Linux Bolivio: [Webinar Comparando Celulares de Linux](https://www.youtube.com/watch?v=IME7w7CZhQ4&t=289s). _I don't speak spanish, but I know that Amos Batto is knowledgable when it comes to Linux on Phones._

#### Shorts
* NOT A FBI Honey Pot: [watch anime from The CLI on your Pinephone!!!!!!](https://www.youtube.com/shorts/sPQg9wHrxKk)
* Jozef Mlich: [glacier-contacts demo](https://www.youtube.com/watch?v=mq14LUck4e8). _Nemo Mobile progress!_

### Something missing?
If your projects' cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!

[^1]: I did not do so, as [working on getting LinuxPhoneApps.org launched](https://twitter.com/linuxphoneapps/status/1504817846622298140) is just way more important.
