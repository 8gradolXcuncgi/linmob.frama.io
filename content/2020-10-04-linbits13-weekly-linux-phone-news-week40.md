+++
title = "LinBits 13: Weekly Linux Phone news / media roundup (week 40)"
aliases = ["2020/10/04/linbits13-weekly-pinephone-news-week40.html", "linbits13"]
author = "Peter"
date = "2020-10-04T00:15:00Z"
layout = "post"
[taxonomies]
tags = ["Pine64", "PinePhone", "Librem 5", "Anbox", "Camera", "Manjaro", "LINMOBapps", "Lomiri", "Phosh", "Megapixels", "Unboxing"]
categories = ["weekly update"]
authors = ["peter"]
+++

_It's sunday. Now what happened since last sunday?_

Autofocus support, further Lomiri images and more. _Commentary in italics._
<!-- more -->


### Software releases and improvements
* [megapixels 0.10.2 landed](https://git.sr.ht/~martijnbraam/megapixels) with [autofocus support](https://twitter.com/braam_martijn/status/1312036122248306689). _You are going to need a proprietary firmware and kernel support to be able to take advantage of this. Danct12 made this work on Arch Linux ARM, but I am too bad at using darktable to judge the results. Still: This progress is amazing!_
* [Manjaro released further Alpha 1 builds of their Lomiri spin for the PinePhone](https://osdn.net/projects/manjaro-arm/storage/pinephone/lomiri/). These include a different variant of the maliit keyboard, which supports GTK apps. _This change broke my install, and on the new install my fix to remove the shadow that pushes GTK apps down and to the right refuses to work. Also,_ `GDK_SCALE=2` _still has no effect. Plasma Mobile apps still need individual launcher-file adjustment, because setting the Qt environment variables globally messes up the Lomiri parts which apparently work different – I wonder whether good Unity 8/Lomiri documentation regarding scaling exists somewhere._ 

### Worth reading
* xnux.eu/log: [New codec driver in my 5.9 kernel](https://xnux.eu/log/#020). _Too many abbrevations for me, but: Progress is always great!_
* Purism: [Software Development Progress July and August 2020](https://puri.sm/posts/software-development-progress-july-and-august-2020/). _If you have a PinePhone and use a distribution that uses Phosh, you should have received all these features a while ago. Also, Purism announced a privacy-focused US-only ["Librem AweSIM service"](https://puri.sm/posts/announcing-librem-awesim-a-privacy-focused-cellular-service-for-the-librem-5/), but I don't know the US cellular market well enough to write anything meaningful._
* redfennec.dev: [Exploring, Part 1: Firmware](https://redfennec.dev/posts/hpp-exploration-1/). _Now that's really diving in._

### Worth watching
* PizzaLovingNerd: [The PinePhone Camera is ALMOST Usable!](https://www.youtube.com/watch?v=t84uF4n0xQ4). _Great video summing up camera progress across distributions._
* Privacy & Security Tips: [Howto: USB-C Functionality Fix Pinephone Modification](https://www.youtube.com/watch?v=FK2hlRQT_b0). _Long time no USB-C mod video._
*  Privacy & Security Tips: [Zoom/Skype Alternatives? Try True E2E Encrypted Video Chat: QTox Pinephone/Desktop), Jitsi And Why](https://www.youtube.com/watch?v=n7sstzeeUxQ). _Nice video which shows QTox on the Pinephone._


#### Unboxing corner
* Liliputing: [Unboxing & First Look: PinePhone postmarketOS Community Edition (Convergence Pack)](https://www.youtube.com/watch?v=r5AGEujqrVw).
* LearnLinuxTV: [Unboxing the Pine64 Pinephone (postmarketOS Edition)](https://www.youtube.com/watch?v=tlZFG070R5E).

#### Convergence Corner
* Purism: [Desktop and Phone Convergence](https://www.youtube.com/watch?v=Mv1CF3qFavw). _Librem 5 flexing its GPU again._
* Avisando: [PinePhone "Desktop Mode"](https://www.youtube.com/watch?v=DD_UNwRGFsU). _I did not even know Plasma Mobile could do that._

### Stuff I did

I had a lot on my list and did not get much done, aside from a [video on Anbox on Manjaro Phosh](https://www.youtube.com/watch?v=8Sha3R4PKSs). This drought in creativity and content creation may continue throughout the next two weeks. 

