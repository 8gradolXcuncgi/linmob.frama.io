+++
title = "LinBits 28: Weekly Linux Phone news / media roundup (week 2)"
aliases = ["2021/01/17/linbits28-weekly-linux-phone-news-week2.html", "linbits28"]
author = "Peter"
date = "2021-01-17T22:45:00Z"
layout = "post"
[taxonomies]
authors = ["peter"]
tags = ["Pine64", "PinePhone", "Arch Linux ARM", "DanctNIX Mobile", "Plasma Mobile", "Phosh", "Purism", "Librem 5", "JingOS", "Plasma Mobile", "AstroSlide", "Mobian", "YARH.IO Micro 2"]
categories = ["weekly update"]

+++

_It's sunday. Now what happened since last sunday?_

Pine64 January update, Mobian Community Edition, Librem5 shipping update and more. _Commentary in italics._
<!-- more -->

### Software development and releases
* FOSS2go: [Portfolio 0.9.4, a simple file manager for linux phones has been released](https://foss2go.com/portfolio-0-9-4-a-simple-file-manager-for-linux-phones-has-been-released/). _Read the [changelog](https://github.com/tchx84/Portfolio/blob/master/CHANGELOG.md) for details._


### Worth reading 

* FOSS2go: [Ubuntu Touch Release Candidate update for PinePhone and PineTab](https://foss2go.com/ubuntu-touch-release-candidate-update-for-pinephone-and-pinetab/).
* LinuxSmartphones: [Genode OS Framework is adding PinePhone support](https://linuxsmartphones.com/genode-os-framework-is-adding-pinephone-support/). _This is not Linux, but that might make it even more interesting!_
* Phoronix: [GTK 4.0.1 Released With Many Bug Fixes, Better Media Acceleration](https://www.phoronix.com/scan.php?page=news_item&px=GTK-4.0.1-Released). _While this does not mean that we now get accelerated GTK4 on the PinePhone, it's nice to see progress._
* OMG!Ubuntu: [First Look: Meet the New Linux Distro Inspired by the iPad](https://www.omgubuntu.co.uk/2021/01/jingos-linux-distro-for-tablets-like-ipados). _[JingOS](https://www.jingos.com/) surely looks nice. It's heavily inspired by iPad OS and [based on Ubuntu and (at least currently) Plasma Mobile](https://forum.jingos.com/t/jingos-roadmap-v1-0-published-on-jan-2021/218). I just hope that they are going to be good open source citizens._

#### The January Update
* PINE64: [January Update: Happy New Gear!](https://www.pine64.org/2021/01/15/january-update-happy-new-gear/). _Nice update for the new year. The (IIRC) delays for some accessories are unfortunate, but Mobian is an excellent choice for the "last Community Edition". Customers are going to have a great out of the box experience! You can pre-order it now._
  * Mobian Blog: [Announcing the PinePhone Mobian Community Edition](https://blog.mobian-project.org/posts/2021/01/15/mobian-community-edition/). _Mobian's part of the announcement. Great that they have a blog now!_
  * LinuxSmartphones: [PinePhone Mobian Community Edition goes up for pre-order Jan 18](https://linuxsmartphones.com/pinephone-mobian-community-edition-goes-up-for-pre-order-jan-18/). _Brad's take._
  * FOSS2go: [Mobian will be the new OS in the PinePhone Community Edition](https://foss2go.com/mobian-will-be-the-new-os-in-the-pinephone-community-edition/). _FOSS2go's take._

#### Purism corner
  
* Purism: [Librem 5 Update: Shipping Estimates and CPU Supply Chain](https://puri.sm/posts/librem-5-update-shipping-estimates-and-cpu-supply-chain/). _I have not received an email yet, but I'll keep you posted._
* Purism: [Purism and Linux 5.9 and Linux 5.10](https://puri.sm/posts/purism-and-linux-5-9-and-5-10/). _Another update on Purism contributions to the Linux kernel, this time on more recent progress._
* Purism: App Spotlight: [Dictonary](https://puri.sm/posts/app-spotlight-dictionary/). _Dictionary is an old hat, but the instructions for hosting a local dictionary are helpful._

#### More Hardware

* LinuxSmartphones: [Crowdfunded Astro Slide 5G smartphone ships in June, has a physical keyboard, and promises Linux support](https://linuxsmartphones.com/crowdfunding-astro-slide-5g-smartphone-ships-in-june-has-a-physical-keyboard-and-promises-linux-support/). _A new device by Planet Computer. For their previous devices their Linux support has usually been Debian + XFCE + some dialer, which according to my sources has been a rather lackluster experience. Here's hope that they are going to do better with this new device._
* Liliputing: [YARH.IO Micro 2 is an even smaller handheld Raspberry Pi PC](https://liliputing.com/2021/01/yarh-io-micro-2-is-an-even-smaller-handheld-raspberry-pi-pc.html). _If you always wanted a relatively small Raspberry Pi 3B+ powered handheld, celebrate now!_
  
#### Usage reports
* Crowdersoup: [Mobian: One Week Of Use](https://crowdersoup.com/post/mobian-one-week-of-use), [Day 1](https://crowdersoup.com/post/mobian-day-one), [Day 2](https://crowdersoup.com/post/mobian-day-two-day-two-mobian-my-pinephone-was-hon), [Day 3](https://crowdersoup.com/post/mobian-day-3-today-was-day-three-running-mobian-da). _Great series on the experience of using a BraveHeart PinePhone with Mobian._
* /u/bloggerdan: Pinephone as a daily driver? A Non-predetermined Amount of Time* with Mobian: [Day 11](https://teddit.net/r/PINE64official/comments/kuprqp/pinephone_as_a_daily_driver_a_nonpredetermined/), [Day 12](https://teddit.net/r/PINE64official/comments/kw8fwj/pinephone_as_a_daily_driver_a_nonpredetermined/), [Day 13](https://teddit.net/r/PINE64official/comments/kxgtlu/pinephone_as_a_daily_driver_a_nonpredetermined/), [Day 14](https://teddit.net/r/PINE64official/comments/kyaj8l/pinephone_as_a_daily_driver_a_nonpredetermined/). _Sad ending on Day 14._

### Worth watching

* PINE64: [January Update: Happy New Gear!](https://tilvids.com/videos/watch/db1e07db-00ed-45c8-97a6-974e3f3a970b).  _The excellent video version of PINE64's January update._ 
* muqtxdir: [Portfolio: simple file manager for linux phones](https://www.youtube.com/watch?v=HSzxQaMGYqs). _A video about Portfolio._
* 太原利州: [pinephone manjaro體驗](https://www.youtube.com/watch?v=z4X-PmYaip4). _A chinese video for a change._
* Mota Khairullah: [PineTab & PinePhone](https://www.youtube.com/watch?v=57zTsjW7C54). _Just another Unboxing._
* Daniel Rodriguez: [iPhone 7 booting Ubuntu 20.04 (to GUI)](https://www.youtube.com/watch?v=DO8vt34kTh0), [iPhone 7 booting Ubuntu 20.04 (to tty/command line](https://www.youtube.com/watch?v=DrntxWqDuvI). _This is thoroughly impressive. Go and read the [reddit](https://teddit.net/r/linux/comments/kux9xx/success_iphone_7_with_dead_nand_netbooting/) [threads](https://teddit.net/r/linux/comments/kvmsfd/success_iphone_7_booting_ubuntu_2004_to_full/) about this topic &mdash; this is a 16 year old dude that took an iPhone with broken NAND storage that went ahead and managed to boot Linux on it! He is going to use it as a webserver. Reduce,_ __Reuse,__ _Recycle!_
* Linux Lounge: [A Look At Ubuntu Touch On The PinePhone](https://peertube.co.uk/videos/watch/3737fdd9-ecfe-4a7d-bce5-90d40c0477f0). _A nice look at Ubuntu Touch on the PinePhone._
* UBports: [Ubuntu Touch Q&A 92](https://www.youtube.com/watch?v=-7NQ1szt7Zk). _In their first Q&A in 2021 they go into many changes that are on the roadmap. Qt 5.12 broke some tests by being a lot faster, the move to 20.04, indicator overhaul. Unfortunately, the kernelupgrade for the PinePhone is not imminent._

### Stuff I did

#### Content

I made and uploaded two new videos:
* "Another look at Manjaro Lomiri on the PinePhone"
  * [PeerTube](https://devtube.dev-wiki.de/videos/watch/094d745f-afcd-413d-b72b-1fd5ea0338e6),
  * [LBRY](https://lbry.tv/@linmob:3/another-look-at-manjaro-lomiri-on-the:c),
  * and [YouTube](https://www.youtube.com/watch?v=nGU6_Lxjw0k);
* and "Revisiting openSUSE on the PinePhone"
  * [PeerTube](https://devtube.dev-wiki.de/videos/watch/ee717e12-5db7-496f-9a21-15eea82e6d9c),
  * [LBRY](https://lbry.tv/@linmob:3/revisiting-opensuse-on-the-pinephone:e),
  * and [YouTube](https://www.youtube.com/watch?v=Zac6WgYIn28).

#### LINMOBapps

I am running a poll to get some information on [what users want for the future of LINMOBapps](https://croodle.wntd.de/#/poll/I3LTpHgmNZ/participation?encryptionKey=athXXWysZWFHxAut5rqpKoBFNudLNoupf45t6AXB). [^1] That aside, just [a few additions and some maintenance happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). Please [do contribute](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/tasks.md)!

#### Random

I played with Plasma Mobile on Manjaro and OpenMandriva, tried to get GNOME Social to build (but it would not compile, even after I had managed to get all the dependencies in place and worked on new content that's not ready yet.

[^1]: If you voted today before 7:00PM UTC, please re-vote. Otherwise your votes on the last three questions are going to be counted as well. I have a backup.
